<?php

namespace Tests\KisphpBundle\Services;

use KisphpBundle\DataTransfer\LayoutTransfer;
use KisphpBundle\Services\CmsManager;
use KisphpBundle\Services\Drivers\DriverInterface;
use PHPUnit\Framework\TestCase;

class CmsManagerTest extends TestCase
{
    public function test_getLayoutContent()
    {
        $cmsManager = $this->createCmsManager();

        $layout = $cmsManager->getLayoutContent(1);

        $this->assertInstanceOf(LayoutTransfer::class, $layout);

        $this->assertEquals(1, count($layout->getRows()));
    }

    public function test_getLayoutContent_inactiveRows()
    {
        $cmsManager = $this->createCmsManager();

        $layout = $cmsManager->getLayoutContent(2);

        $this->assertInstanceOf(LayoutTransfer::class, $layout);
        $this->assertEquals(0, count($layout->getRows()));
    }

    public function test_getLayoutContent_inherited_column()
    {
        $cmsManager = $this->createCmsManager();

        $layout = $cmsManager->getLayoutContent(3);

        $this->assertInstanceOf(LayoutTransfer::class, $layout);
        $this->assertEquals(1, count($layout->getRows()));

        $this->assertEquals(1, $layout->getRows()[3]->getColumns()[1]->getId());
    }

    public function test_getLayoutContent_empty()
    {
        $cmsManager = $this->createCmsManager();

        $this->assertNull($cmsManager->getLayoutContent(1000));
    }

    public function test_createLayoutContent()
    {
        $cmsManager = $this->createCmsManager();

        $layout = $cmsManager->getLayoutContent(1);

        $cmsManager->createLayoutContent($layout);

        $this->assertNotNull($layout->getContent());
    }

    /**
     * @return \KisphpBundle\Services\Drivers\DriverInterface|\Mockery\MockInterface
     */
    protected function mockDriver()
    {
        $driver = \Mockery::mock(DriverInterface::class);
        // generate layout 1 functional
        $driver->shouldReceive('getLayoutById')->with(1)->andReturn([
            'id' => 1,
            'status' => 2,
            'title' => 'test layout 1',
            'url' => '/',
            'seo_title' => 'seo title',
            'seo_keywords' => 'seo keywords',
            'seo_description' => 'seo description',
            'rows' => [
                [
                    'id' => 1,
                    'id_parent' => 0,
                    'status' => 2,
                ],
            ],
        ]);
        $driver->shouldReceive('getLayoutRows')->with(1)->andReturn([
            [
                'id' => 1,
                'id_parent' => null,
                'id_layout' => 1,
                'type' => null,
                'status' => 2,
                'title' => 'row 1',
                'css_class' => 'row-class-1',
                'css_id' => '',
            ]
        ]);

        $column_1 = [
            'id' => 1,
            'id_row' => 1,
            'id_parent' => null,
            'title' => 'column 1',
            'attr_class' => 'col-class-1',
        ];

        $driver->shouldReceive('getRowColumns')->with(1)->andReturn([
            $column_1
        ]);
        $widget_1 = [
            'id' => 1,
            'id_column' => 1,
            'status' => 2,
            'change_status' => 0,
            'position' => 1,
            'title' => 'widget 1',
            'type' => 'widget.type.text',
            'date_start' => null,
            'date_stop' => null,
            'content' => json_encode(['text' => 'widget content 1']),
        ];
        $widget_2 = [
            'id' => 2,
            'id_column' => 1,
            'status' => 2,
            'change_status' => 0,
            'position' => 1,
            'title' => 'widget 1',
            'type' => '',
            'date_start' => null,
            'date_stop' => null,
            'content' => json_encode(['text' => 'widget content 1']),
        ];
        $widget_3 = [
            'id' => 3,
            'id_column' => 1,
            'status' => 1,
            'change_status' => 0,
            'position' => 1,
            'title' => 'widget 1',
            'type' => 'widget.type.text',
            'date_start' => null,
            'date_stop' => null,
            'content' => json_encode(['text' => 'widget content 1']),
        ];
        $driver->shouldReceive('getColumnWidgets')->with(1)->andReturn([
            $widget_1,
            $widget_2,
            $widget_3,
        ]);

        $driver->shouldReceive('getColumnById')->with(1)->andReturn($column_1);

        // generate layout 1000 not existant
        $driver->shouldReceive('getLayoutById')->with(1000)->andReturnFalse();

        // generate layout 2 with inactive rows
        $driver->shouldReceive('getLayoutById')->with(2)->andReturn([
            'id' => 2,
            'status' => 2,
            'title' => 'test layout 2',
            'url' => '/',
            'seo_title' => 'seo title',
            'seo_keywords' => 'seo keywords',
            'seo_description' => 'seo description',
            'rows' => [
                [
                    'id' => 2,
                    'status' => 1,
                    'id_parent' => null,
                ],
            ],
        ]);
        $driver->shouldReceive('getLayoutRows')->with(2)->andReturn([
            [
                'id' => 1,
                'id_layout' => 1,
                'id_parent' => null,
                'status' => 1,
                'type' => null,
                'title' => 'row 1',
                'css_class' => 'row-class-1',
                'css_id' => '',
            ]
        ]);

        $driver->shouldReceive('getLayoutRows')->with(3)->andReturn([
            [
                'id' => 3,
                'id_layout' => 3,
                'id_parent' => null,
                'status' => 2,
                'type' => null,
                'title' => 'row 3',
                'css_class' => 'row-class-1',
                'css_id' => '',
            ]
        ]);

        $driver->shouldReceive('getRowColumns')->with(3)->andReturn([
            [
                'id' => 3,
                'id_row' => 3,
                'id_parent' => 1,
                'title' => 'column 1',
                'attr_class' => 'col-class-1',
            ]
        ]);

        // get widget 1 content
        $driver->shouldReceive('getWidgetContent')->withAnyArgs()->andReturn([]);

        // generate layout 3 with inherited column
        $driver->shouldReceive('getLayoutById')->with(3)->andReturn([
            'id' => 3,
            'status' => 2,
            'title' => 'test layout 3',
            'url' => '/homepage',
            'seo_title' => 'seo title',
            'seo_keywords' => 'seo keywords',
            'seo_description' => 'seo description',
            'rows' => [
                [
                    'id' => 3,
                    'status' => 1,
                    'id_parent' => null,
                ],
            ],
        ]);

        return $driver;
    }

    /**
     * @return \KisphpBundle\Services\CmsManager
     */
    protected function createCmsManager(): \KisphpBundle\Services\CmsManager
    {
        $twig = \Mockery::mock(\Twig_Environment::class);
        $twig->shouldReceive('render')->withAnyArgs()->andReturn('rendered content');

        $driver = $this->mockDriver();

        $cmsManager = new CmsManager($twig, $driver);

        return $cmsManager;
    }
}
