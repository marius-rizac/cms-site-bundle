<?php

namespace Tests\KisphpBundle\Services;

use KisphpBundle\DataTransfer\LayoutTransfer;
use KisphpBundle\Services\CmsManager;
use KisphpBundle\Services\CmsRowFunctionService;
use KisphpBundle\Services\Drivers\AbstractDriver;
use KisphpBundle\Services\Drivers\DriverInterface;
use KisphpBundle\Services\WidgetContentFactory;
use KisphpBundle\Services\Widgets\ContentNone;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Router;

class WidgetContetFactoryTest extends TestCase
{
    public function test_create_driver_not_existant()
    {
        $driver = $this->createWidgetContentFactory()->create('demo');

        self::assertInstanceOf(ContentNone::class, $driver);
    }

    public function test_create_driver_existent()
    {
        $driver = $this->createWidgetContentFactory()->create('none');

        self::assertInstanceOf(ContentNone::class, $driver);

        self::assertSame([], $driver->getContent([], new LayoutTransfer()));
    }

    /**
     * @return \KisphpBundle\Services\WidgetContentFactory
     */
    protected function createWidgetContentFactory(): \KisphpBundle\Services\WidgetContentFactory
    {
        $routing = \Mockery::mock(Router::class);
        $container = \Mockery::mock(ContainerInterface::class);
        $cmsManager = \Mockery::mock(CmsManager::class);
        $cmsRowFuntionService = \Mockery::mock(CmsRowFunctionService::class);
        $requestStack = \Mockery::mock(RequestStack::class);
        $requestStack->shouldReceive('getMasterRequest')->withNoArgs()->andReturn(new Request());
        
        $container->shouldReceive('get')->withArgs(['router'])->andReturn($routing);
        $container->shouldReceive('get')->withArgs(['cms.manager'])->andReturn($cmsManager);
        $container->shouldReceive('get')->withArgs(['cms.row.function.service'])->andReturn($cmsRowFuntionService);
        $container->shouldReceive('get')->withArgs(['request_stack'])->andReturn($requestStack);
        $persistenceDriver = \Mockery::mock(AbstractDriver::class);
        $container->shouldReceive('get')->withArgs(['persistence.driver'])->andReturn($persistenceDriver);

        return new WidgetContentFactory($container);
    }
}
