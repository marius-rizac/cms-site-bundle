<?php

namespace Tests\KisphpBundle\Twig;

use KisphpBundle\Services\CmsManager;
use KisphpBundle\Services\CmsRowFunctionService;
use KisphpBundle\Services\Drivers\AbstractDriver;
use KisphpBundle\Twig\KisphpTwigExtensions;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Router;

class KisphpTwigExtensionsTest extends TestCase
{
    public function test_extension()
    {
        $ext = $this->createExtension();

        $this->assertNotEmpty($ext->getFunctions());
        $this->assertNotEmpty($ext->getFilters());
    }

    /**
     * @return \KisphpBundle\Twig\KisphpTwigExtensions
     */
    protected function createExtension()
    {
        $routing = \Mockery::mock(Router::class);
        $container = \Mockery::mock(ContainerInterface::class);
        $cmsManager = \Mockery::mock(CmsManager::class);
        $cmsRowFuntionService = \Mockery::mock(CmsRowFunctionService::class);
        $requestStack = \Mockery::mock(RequestStack::class);
        $requestStack->shouldReceive('getMasterRequest')->withNoArgs()->andReturn(new Request());

        $container->shouldReceive('get')->withArgs(['router'])->andReturn($routing);
        $container->shouldReceive('get')->withArgs(['cms.manager'])->andReturn($cmsManager);
        $container->shouldReceive('get')->withArgs(['cms.row.function.service'])->andReturn($cmsRowFuntionService);
        $container->shouldReceive('get')->withArgs(['request_stack'])->andReturn($requestStack);
        $persistenceDriver = \Mockery::mock(AbstractDriver::class);
        $container->shouldReceive('get')->withArgs(['persistence.driver'])->andReturn($persistenceDriver);

        return new KisphpTwigExtensions($container, new Request());
    }
}
