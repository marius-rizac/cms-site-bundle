<?php

namespace Tests\KisphpBundle\Twig;

use KisphpBundle\Twig\Filters\TruncateFilter;
use PHPUnit\Framework\TestCase;

class TruncateFilterTest extends TestCase
{
    public function test_truncate_filter()
    {
        $filter = new TruncateFilter();

        $callable = $filter->getExtension()->getCallable();

        self::assertSame('Const...', $callable('Constantinopol', 5));
    }
}
