<?php

namespace Tests\KisphpBundle\Twig;

use KisphpBundle\DataTransfer\LayoutRowTransfer;
use KisphpBundle\Services\CmsManager;
use KisphpBundle\Twig\Functions\Cms\CmsRowBeginFunction;
use KisphpBundle\Twig\Functions\Cms\CmsRowEndFunction;
use KisphpBundle\Twig\Functions\Cms\CmsRowFunctionInterface;
use KisphpBundle\Twig\Functions\MatchFunction;
use KisphpBundle\Twig\Functions\ShowCmsColumnFunction;
use KisphpBundle\Twig\Functions\ThumbFunction;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;

class TwigFunctionsTest extends TestCase
{
    public function test_match_function()
    {
        $filter = new MatchFunction();

        $callable = $filter->getExtension()->getCallable();

        self::assertEquals(1, $callable('alfa', 'betalfagama'));
    }

    public function test_match_function_not_found()
    {
        $filter = new MatchFunction();

        $callable = $filter->getExtension()->getCallable();

        self::assertEquals(0, $callable('omega', 'betalfagama'));
    }

    public function test_thumb_function()
    {
        $routing = \Mockery::mock(Router::class);
        $routing->shouldReceive('generate')->withAnyArgs()->andReturnNull();

        $filter = new ThumbFunction($routing);

        $callable = $filter->getExtension()->getCallable();

        self::assertNull($callable('aaa', 20, 20, 'pic.png'));
    }
}
