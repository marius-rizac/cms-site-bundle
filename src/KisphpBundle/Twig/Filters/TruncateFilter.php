<?php

namespace KisphpBundle\Twig\Filters;

use Kisphp\Twig\AbstractTwigFilter;

class TruncateFilter extends AbstractTwigFilter
{
    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'truncate';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function ($text, $limit, $extra = '...') {
            $newTextStripped = strip_tags($text);
            $newText = html_entity_decode($newTextStripped);
            $newText = substr($newText, 0, $limit);

            if (strlen($newTextStripped) > $limit) {
                $newText .= $extra;
            }

            return $newText;
        };
    }
}
