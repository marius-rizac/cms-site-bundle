<?php

namespace KisphpBundle\Twig\Functions\Cms;

use KisphpBundle\DataTransfer\LayoutRowTransfer;

interface CmsRowFunctionInterface
{
    /**
     * @param \KisphpBundle\DataTransfer\LayoutRowTransfer $rowTransfer
     *
     * @return string
     */
    public function getCmsRowBeginHtml(LayoutRowTransfer $rowTransfer);

    /**
     * @param \KisphpBundle\DataTransfer\LayoutRowTransfer $rowTransfer
     *
     * @return mixed
     */
    public function getCmsRowEndHtml(LayoutRowTransfer $rowTransfer);
}
