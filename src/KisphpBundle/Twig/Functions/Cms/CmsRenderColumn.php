<?php

namespace KisphpBundle\Twig\Functions\Cms;

use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;
use KisphpBundle\Services\CmsManager;

class CmsRenderColumn extends AbstractTwigFunction
{
    use isSafeHtml;

    /**
     * @var \KisphpBundle\Services\CmsManager
     */
    protected $cmsManager;

    /**
     * @param \KisphpBundle\Services\CmsManager $cmsManager
     */
    public function __construct(CmsManager $cmsManager)
    {
        parent::__construct();

        $this->cmsManager = $cmsManager;
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'cmsRenderColumn';
    }

    /**
     * @return callable|\Closure
     */
    protected function getExtensionCallback()
    {
        return function ($columnId) {
            $widgets = $this->cmsManager->getColumnWidgets($columnId);

            $output = [];
            foreach ($widgets as $widget) {
                $output[] = $this->cmsManager->render($widget);
            }

            return implode(' ', $output);
        };
    }
}
