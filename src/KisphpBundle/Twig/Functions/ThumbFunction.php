<?php

namespace KisphpBundle\Twig\Functions;

use Kisphp\Twig\AbstractTwigFunction;
use Symfony\Component\Routing\Router;

class ThumbFunction extends AbstractTwigFunction
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;

        parent::__construct();
    }

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'thumb';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        /*
         * @param string $dirname
         * @param int $width
         * @param int $height
         * @param string $filename
         *
         * @return string
         */
        return function ($dirname, $width, $height, $filename) {
            return $this->generateImageUrl($dirname, $width, $height, $filename);
        };
    }

    /**
     * @param string $dirname
     * @param int $width
     * @param int $height
     * @param string $filename
     *
     * @throws \Symfony\Component\Routing\Exception\RouteNotFoundException
     * @throws \Symfony\Component\Routing\Exception\MissingMandatoryParametersException
     * @throws \Symfony\Component\Routing\Exception\InvalidParameterException
     *
     * @return string
     */
    protected function generateImageUrl($dirname, $width, $height, $filename)
    {
        return $this->router->generate('thumbs', [
            'directory' => $dirname,
            'width' => $width,
            'height' => $height,
            'filename' => $filename,
        ]);
    }
}
