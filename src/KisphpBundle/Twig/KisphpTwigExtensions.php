<?php

namespace KisphpBundle\Twig;

use KisphpBundle\Twig\Filters\TruncateFilter;
use KisphpBundle\Twig\Functions\Cms\CmsRenderColumn;
use KisphpBundle\Twig\Functions\Cms\CmsRowBeginFunction;
use KisphpBundle\Twig\Functions\Cms\CmsRowEndFunction;
use KisphpBundle\Twig\Functions\MatchFunction;
use KisphpBundle\Twig\Functions\ThumbFunction;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class KisphpTwigExtensions extends AbstractExtension
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            TruncateFilter::create(),
        ];
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            $this->createThumbFunction(),
            MatchFunction::create(),
//            $this->createCmsRowBeginFunction(),
//            $this->createCmsRowEndFunction(),
            $this->createCmsRenderColumnFunction(),
        ];
    }

    /**
     * @return TwigFunction
     */
    protected function createCmsRenderColumnFunction()
    {
        return (new CmsRenderColumn($this->container->get('cms.manager')))->getExtension();
    }

    /**
     * @return TwigFunction
     */
    protected function createCmsRowBeginFunction()
    {
        return (new CmsRowBeginFunction($this->container->get('cms.row.function.service')))->getExtension();
    }

    /**
     * @return TwigFunction
     */
    protected function createCmsRowEndFunction()
    {
        return (new CmsRowEndFunction($this->container->get('cms.row.function.service')))->getExtension();
    }

    /**
     * @return TwigFunction
     */
    protected function createThumbFunction()
    {
        return (new ThumbFunction($this->container->get('router')))->getExtension();
    }
}
