<?php

namespace KisphpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CmsController extends Controller
{
    const KISPHP_CMS_TEMPLATE = 'KisphpBundle:Cms:index.html.twig';
    const DATA_LAYOUT = 'layout';

    /**
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \KisphpBundle\Exceptions\CmsLayoutNotFound
     */
    public function indexAction($id = 0)
    {
        return $this->showLayoutContent($id);
    }

    /**
     * @param int $layoutId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function showLayoutContent($layoutId)
    {
        $cmsManager = $this->get('cms.manager');
        $layoutTransfer = $cmsManager
            ->getLayoutContent($layoutId)
        ;

        if ($layoutTransfer !== null) {
            $cmsManager->createLayoutContent($layoutTransfer);
        }

        return $this->render(static::KISPHP_CMS_TEMPLATE, [
            static::DATA_LAYOUT => $layoutTransfer,
        ]);
    }
}
