<?php

namespace KisphpBundle\DataTransfer;

class LayoutColumnWidgetTransfer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $idColumn = 0;

    /**
     * @var \KisphpBundle\DataTransfer\LayoutColumnTransfer
     */
    protected $column;

    /**
     * @var int
     */
    protected $status = 1;

    /**
     * @var int
     */
    protected $changeStatus = 0;

    /**
     * @var int
     */
    protected $position = 1000;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $type = '';

    /**
     * @var \DateTime
     */
    protected $dateStart;

    /**
     * @var \DateTime
     */
    protected $dateStop;

    /**
     * @var array
     */
    protected $content = [];

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdColumn()
    {
        return $this->idColumn;
    }

    /**
     * @param int $idColumn
     */
    public function setIdColumn($idColumn)
    {
        $this->idColumn = $idColumn;
    }

    /**
     * @return \KisphpBundle\DataTransfer\LayoutColumnTransfer
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutColumnTransfer $column
     */
    public function setColumn(LayoutColumnTransfer $column)
    {
        $this->column = $column;
    }

    /**
     * @return int
     */
    public function getChangeStatus()
    {
        return $this->changeStatus;
    }

    /**
     * @param int $changeStatus
     */
    public function setChangeStatus($changeStatus)
    {
        $this->changeStatus = $changeStatus;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateStop()
    {
        return $this->dateStop;
    }

    /**
     * @param \DateTime $dateStop
     */
    public function setDateStop($dateStop)
    {
        $this->dateStop = $dateStop;
    }

    /**
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setContent(array $content)
    {
        $this->content = $content;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }
}
