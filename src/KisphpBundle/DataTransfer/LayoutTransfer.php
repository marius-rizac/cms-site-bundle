<?php

namespace KisphpBundle\DataTransfer;

use Kisphp\Utils\Status;

class LayoutTransfer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $status = Status::INACTIVE;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $url = '';

    /**
     * @var string
     */
    protected $seoTitle = '';

    /**
     * @var string
     */
    protected $seoKeywords = '';

    /**
     * @var string
     */
    protected $seoDescription = '';

    /**
     * @var array LayoutRowTransfer[]
     */
    protected $rows = [];

    /**
     * @var array
     */
    protected $content = [];

    /**
     * @param int $widgetId
     * @param string $content
     */
    public function addContent($widgetId, $content)
    {
        $this->content[$widgetId] = $content;
    }

    /**
     * @param string $seoTitle
     */
    public function appendSeoTitle($seoTitle)
    {
        $this->seoTitle .= $seoTitle;
    }

    /**
     * @param string $seoKeywords
     */
    public function appendSeoKeywords($seoKeywords)
    {
        $this->seoKeywords .= $seoKeywords;
    }

    /**
     * @param string $seoDescription
     */
    public function appendSeoDescription($seoDescription)
    {
        $this->seoDescription .= $seoDescription;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutRowTransfer $rowTransfer
     */
    public function addRow(LayoutRowTransfer $rowTransfer)
    {
        $this->rows[$rowTransfer->getId()] = $rowTransfer;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @param string $seoTitle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @return string
     */
    public function getSeoKeywords()
    {
        return $this->seoKeywords;
    }

    /**
     * @param string $seoKeywords
     */
    public function setSeoKeywords($seoKeywords)
    {
        $this->seoKeywords = $seoKeywords;
    }

    /**
     * @return string
     */
    public function getSeoDescription()
    {
        return $this->seoDescription;
    }

    /**
     * @param string $seoDescription
     */
    public function setSeoDescription($seoDescription)
    {
        $this->seoDescription = $seoDescription;
    }

    /**
     * @return array
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * @return array
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param array $content
     */
    public function setContent(array $content)
    {
        $this->content = $content;
    }
}
