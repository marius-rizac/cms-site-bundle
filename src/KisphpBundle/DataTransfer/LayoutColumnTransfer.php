<?php

namespace KisphpBundle\DataTransfer;

class LayoutColumnTransfer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $id_row;

    /**
     * @var string
     */
    protected $attr_class;

    /**
     * @var \KisphpBundle\DataTransfer\LayoutRowTransfer
     */
    protected $rowTransfer;

    /**
     * @var \KisphpBundle\DataTransfer\LayoutColumnWidgetTransfer[]
     */
    protected $widgets = [];

    /**
     * @return \KisphpBundle\DataTransfer\LayoutRowTransfer
     */
    public function getRowTransfer(): LayoutRowTransfer
    {
        return $this->rowTransfer;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutRowTransfer $rowTransfer
     */
    public function setRowTransfer(LayoutRowTransfer $rowTransfer)
    {
        $this->rowTransfer = $rowTransfer;
    }

    /**
     * @return string
     */
    public function getAttrClass(): string
    {
        return $this->attr_class;
    }

    /**
     * @param string $attr_class
     */
    public function setAttrClass(string $attr_class): void
    {
        $this->attr_class = $attr_class;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdRow()
    {
        return $this->id_row;
    }

    /**
     * @param int $id_row
     */
    public function setIdRow($id_row)
    {
        $this->id_row = $id_row;
    }

    /**
     * @return \KisphpBundle\DataTransfer\LayoutColumnWidgetTransfer[]
     */
    public function getWidgets()
    {
        return $this->widgets;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutColumnWidgetTransfer $transfer
     */
    public function addWidget(LayoutColumnWidgetTransfer $transfer)
    {
        $this->widgets[$transfer->getId()] = $transfer;
    }
}
