<?php

namespace KisphpBundle\DataTransfer;

class CmsTransferFactory
{
    /**
     * @param array $layout
     *
     * @return \KisphpBundle\DataTransfer\LayoutTransfer
     */
    public static function createLayoutTransfer(array $layout)
    {
        $transfer = new LayoutTransfer();
        $transfer->setId($layout['id']);
        $transfer->setStatus((int) $layout['status']);
        $transfer->setTitle($layout['title']);
        $transfer->setUrl($layout['url']);
        $transfer->setSeoTitle($layout['seo_title']);
        $transfer->setSeoKeywords($layout['seo_keywords']);
        $transfer->setSeoDescription($layout['seo_description']);

        return $transfer;
    }

    /**
     * @param array $row
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $layoutTransfer
     *
     * @return \KisphpBundle\DataTransfer\LayoutRowTransfer
     */
    public static function createLayoutRowTransfer(array $row, LayoutTransfer $layoutTransfer)
    {
        $transfer = new LayoutRowTransfer();
        $transfer->setId($row['id']);
        $transfer->setIdParent($row['id_parent']);
        $transfer->setTitle($row['title']);
        $transfer->setCssClass($row['css_class']);
        $transfer->setCssId($row['css_id']);
        $transfer->setType($row['type']);
        $transfer->setLayout($layoutTransfer);

        return $transfer;
    }

    /**
     * @param array $column
     * @param \KisphpBundle\DataTransfer\LayoutRowTransfer $rowTransfer
     *
     * @return \KisphpBundle\DataTransfer\LayoutColumnTransfer
     */
    public static function createLayoutColumnTransfer(array $column, LayoutRowTransfer $rowTransfer)
    {
        $transfer = new LayoutColumnTransfer();
        $transfer->setId($column['id']);
        $transfer->setIdRow($column['id_row']);
        $transfer->setAttrClass($column['attr_class']);
        $transfer->setRowTransfer($rowTransfer);

        return $transfer;
    }

    /**
     * @param array $widget
     * @param \KisphpBundle\DataTransfer\LayoutColumnTransfer $columnTransfer
     *
     * @return \KisphpBundle\DataTransfer\LayoutColumnWidgetTransfer
     */
    public static function createWidgetTransfer(array $widget, LayoutColumnTransfer $columnTransfer)
    {
        $transfer = new LayoutColumnWidgetTransfer();
        $transfer->setId((int) $widget['id']);
        $transfer->setIdColumn((int) $widget['id_column']);
        $transfer->setColumn($columnTransfer);
        $transfer->setStatus((int) $widget['status']);
        $transfer->setChangeStatus((int) $widget['change_status']);
        $transfer->setPosition($widget['position']);
        $transfer->setTitle($widget['title']);
        $transfer->setType($widget['type']);
        $transfer->setDateStart($widget['date_start']);
        $transfer->setDateStop($widget['date_stop']);
        if ($widget['content'] !== null) {
            $transfer->setContent(json_decode($widget['content'], true));
        }

        return $transfer;
    }
}
