<?php

namespace KisphpBundle\DataTransfer;

class LayoutRowTransfer
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $id_parent;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $type = '';

    /**
     * @var string
     */
    protected $css_class = '';

    /**
     * @var string
     */
    protected $css_id = '';

    /**
     * @var array LayoutColumnTransfer[]
     */
    protected $columns = [];

    /**
     * @var \KisphpBundle\DataTransfer\LayoutTransfer
     */
    protected $layout;

    /**
     * @return \KisphpBundle\DataTransfer\LayoutTransfer
     */
    public function getLayout(): LayoutTransfer
    {
        return $this->layout;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $layout
     */
    public function setLayout(LayoutTransfer $layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getIdParent()
    {
        return $this->id_parent;
    }

    /**
     * @param int $id_parent
     */
    public function setIdParent($id_parent)
    {
        $this->id_parent = $id_parent;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCssClass()
    {
        return $this->css_class;
    }

    /**
     * @param string $css_class
     */
    public function setCssClass($css_class)
    {
        $this->css_class = $css_class;
    }

    /**
     * @return string
     */
    public function getCssId()
    {
        return $this->css_id;
    }

    /**
     * @param string $css_id
     */
    public function setCssId($css_id)
    {
        $this->css_id = $css_id;
    }

    /**
     * @return \KisphpBundle\DataTransfer\LayoutColumnTransfer[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutColumnTransfer[] $columns
     */
    public function setColumns(array $columns)
    {
        $this->columns = $columns;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutColumnTransfer $columnTransfer
     */
    public function addColumn(LayoutColumnTransfer $columnTransfer)
    {
        $this->columns[$columnTransfer->getId()] = $columnTransfer;
    }
}
