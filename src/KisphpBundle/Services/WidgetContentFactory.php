<?php

namespace KisphpBundle\Services;

use Psr\Container\ContainerInterface;

class WidgetContentFactory
{
    /**
     * @var \Psr\Container\ContainerInterface
     */
    protected $container;

    /**
     * @param \Psr\Container\ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param string $widgetType
     *
     * @return \KisphpBundle\Services\Widgets\AbstractContent
     */
    public function create($widgetType)
    {
        $namespacePath = 'KisphpBundle\\Services\\Widgets\\%s';

        $namespace = sprintf($namespacePath, $this->createTypeClass($widgetType));

        if (class_exists($namespace)) {
            return new $namespace($this->container);
        }

        $namespaceNull = sprintf($namespacePath, $this->createTypeClass('None'));

        return new $namespaceNull($this->container);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function createTypeClass($name)
    {
        $name = str_replace('_', ' ', $name);
        $name = ucwords($name);
        $name = str_replace(' ', '', $name);

        return 'Content' . $name;
    }
}
