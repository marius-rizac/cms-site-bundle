<?php

namespace KisphpBundle\Services\Drivers;

use KisphpBundle\DataTransfer\LayoutTransfer;
use KisphpBundle\Services\WidgetContentFactory;
use Psr\Container\ContainerInterface;

abstract class AbstractDriver implements DriverInterface
{
    /**
     * @var \Psr\Container\ContainerInterface
     */
    protected $container;

    /**
     * @var WidgetContentFactory
     */
    protected $widgetContentFactory;

    /**
     * @param \Psr\Container\ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;

        $this->widgetContentFactory = $container->get('widget.content.factory');
    }

    /**
     * @param string $type
     * @param array $widgetData
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $layoutTransfer
     *
     * @return mixed
     */
    protected function createWidgetContent($type, array $widgetData, LayoutTransfer $layoutTransfer)
    {
        /** @var \KisphpBundle\Services\Widgets\AbstractContent $type */
        $type = $this->widgetContentFactory->create($type);

        return $type->getContent($widgetData, $layoutTransfer);
    }
}
