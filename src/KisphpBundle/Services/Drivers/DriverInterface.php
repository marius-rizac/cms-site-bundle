<?php

namespace KisphpBundle\Services\Drivers;

use KisphpBundle\DataTransfer\LayoutTransfer;

interface DriverInterface
{
    /**
     * @param int $idColumn
     *
     * @return array
     */
    public function getColumnWidgets($idColumn);

    /**
     * @param array $widgetData
     * @param string $widgetName
     *
     * @return array
     */
    public function getWidgetContent(array $widgetData, $widgetName, LayoutTransfer $layoutTransfer);

    /**
     * @param int $idLayout
     *
     * @return array
     */
    public function getLayoutById($idLayout);

    /**
     * @param int $idLayout
     *
     * @return array
     */
    public function getLayoutRows($idLayout);

    /**
     * @param int $idRow
     *
     * @return array
     */
    public function getRowColumns($idRow);

    /**
     * @param int $id
     *
     * @return array
     */
    public function getColumnById($id);
}
