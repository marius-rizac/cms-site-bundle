<?php

namespace KisphpBundle\Services\Widgets;

use KisphpBundle\DataTransfer\LayoutTransfer;
use KisphpBundle\Services\Drivers\DriverInterface;
use Psr\Container\ContainerInterface;

abstract class AbstractContent
{
    /**
     * @var DriverInterface
     */
    protected $driver;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    protected $request;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @param \Psr\Container\ContainerInterface $container
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->driver = $container->get('persistence.driver');
        $this->request = $container->get('request_stack')->getMasterRequest();
    }

    /**
     * @param array $widgetData
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $layoutTransfer
     *
     * @return array
     */
    abstract public function getContent(array $widgetData, LayoutTransfer $layoutTransfer);
}
