<?php

namespace KisphpBundle\Services\Widgets;

use KisphpBundle\DataTransfer\LayoutTransfer;

class ContentNone extends AbstractContent
{
    /**
     * @param array $widgetData
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $layoutTransfer
     *
     * @return array
     */
    public function getContent(array $widgetData, LayoutTransfer $layoutTransfer)
    {
        return [];
    }
}
