<?php

namespace KisphpBundle\Services;

use Kisphp\Utils\Status;
use KisphpBundle\DataTransfer\CmsTransferFactory;
use KisphpBundle\DataTransfer\LayoutTransfer;
use KisphpBundle\Services\Drivers\DriverInterface;

class CmsManager
{
    const WIDGETS_NAMESPACE = '@Kisphp/widgets/';
    const WIDGETS_TEMPLATE_EXTENSION = '.html.twig';

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var \KisphpBundle\Services\Drivers\DriverInterface
     */
    protected $driver;

    /**
     * @var \KisphpBundle\DataTransfer\LayoutTransfer
     */
    protected $layoutTransfer;

    /**
     * @param \Twig_Environment $twig
     * @param \KisphpBundle\Services\Drivers\DriverInterface $driver
     */
    public function __construct(\Twig_Environment $twig, DriverInterface $driver)
    {
        $this->twig = $twig;
        $this->driver = $driver;
    }

    /**
     * @param int $layoutId
     *
     * @return null|\KisphpBundle\DataTransfer\LayoutTransfer
     */
    public function getLayoutContent($layoutId)
    {
        $content = $this->driver->getLayoutById($layoutId);
        if ($content === false) {
            return null;
        }

        $this->layoutTransfer = CmsTransferFactory::createLayoutTransfer($content);

        $rows = $this->driver->getLayoutRows($layoutId);
        foreach ($rows as $row) {
            if ((int) $row['status'] !== Status::ACTIVE) {
                continue;
            }
            $rowContent = CmsTransferFactory::createLayoutRowTransfer($row, $this->layoutTransfer);

            $columns = $this->driver->getRowColumns($row['id']);
            foreach ($columns as $col) {
                if ($col['id_parent'] > 0) {
                    $col = $this->driver->getColumnById($col['id_parent']);
                }

                $columnTransfer = CmsTransferFactory::createLayoutColumnTransfer($col, $rowContent);
                $rowContent->addColumn($columnTransfer);
                $widgets = $this->driver->getColumnWidgets($columnTransfer->getId());

                foreach ($widgets as $widget) {
                    $widgetTransfer = CmsTransferFactory::createWidgetTransfer($widget, $columnTransfer);
                    $columnTransfer->addWidget($widgetTransfer);
                }
            }

            $this->layoutTransfer->addRow($rowContent);
        }

        return $this->layoutTransfer;
    }

    /**
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $layoutTransfer
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function createLayoutContent(LayoutTransfer $layoutTransfer)
    {
        /** @var \KisphpBundle\DataTransfer\LayoutRowTransfer $row */
        foreach ($layoutTransfer->getRows() as $row) {
            foreach ($row->getColumns() as $column) {
                $widgets = $this->getColumnWidgets($column->getId());

                foreach ($widgets as $widget) {
                    if ((int) $widget['status'] !== Status::ACTIVE) {
                        continue;
                    }

                    $layoutTransfer->addContent($widget['id'], $this->render($widget));
                }
            }
        }
    }

    /**
     * @param int $idColumn
     *
     * @return array
     */
    public function getColumnWidgets($idColumn)
    {
        return $this->driver->getColumnWidgets($idColumn);
    }

    /**
     * @param array $widgetData
     *
     * @return string
     *
     * @throws \Twig_Error_Syntax
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Loader
     */
    public function render(array $widgetData)
    {
        $widgetName = $this->filterType($widgetData['type']);
        $layout = static::WIDGETS_NAMESPACE
            . $widgetName
            . static::WIDGETS_TEMPLATE_EXTENSION;

        $widgetContent = $this->driver->getWidgetContent($widgetData, $widgetName, $this->layoutTransfer);

        return $this->twig->render(
            $layout,
            $widgetContent
        );
    }

    /**
     * @param string $type
     *
     * @return string
     */
    protected function filterType($type)
    {
        $type = str_replace([
            'widget.type.',
            'type_',
        ], '', strtolower($type));

        if (empty($type)) {
            $type = 'null';
        }

        return $type;
    }
}
