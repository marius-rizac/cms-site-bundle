# Kisphp CMS Site Bundle

Kisphp bundle that displays CMS pages in site

## Installation

```bash
composer require kisphp/cms-site-bundle
```

## Add widget template

Create file `app/Resources/KisphpBundle/views/widgets/cms_template_name.html.twig`


## Populate widget

Create a file like the next example:

```bash
<?php

namespace KisphpBundle\Services\Widgets;

use KisphpBundle\Services\Drivers\MysqlDriver;
use KisphpBundle\DataTransfer\LayoutTransfer;

class ContentCmsTemplateName extends AbstractContent
{
    /**
     * @var MysqlDriver
     */
    protected $driver;

    /**
     * @param array $widgetData
     * @param \KisphpBundle\DataTransfer\LayoutTransfer $transfer
     *
     * @return array
     */
    public function getContent(array $widgetData, LayoutTransfer $transfer) : array
    {
        $idProduct = $widgetData['content']['id_product'];

        $product = $this->driver->getProductById($idProduct);

        // optional if you want to add meta tags on page
        $transfer->setSeoTitle($product['seo_title']);
        $transfer->setSeoKeywords($product['seo_keywords']);
        $transfer->setSeoDescription($product['seo_description']);

        return $product;
    }
}
```

> Notice that the class name must begin with `Content` and must extend `AbstractContent` class

> Driver property is from persistence layer and it can be a connector to database, redis, elasticsearch, etc

## Add necessary services

```yaml
persistence.driver:
    class: KisphpBundle\Services\Drivers\MysqlDriver
    public: true
    arguments:
        - "@service_container"
```
