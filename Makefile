.PHONY: t fix

t:
	XDEBUG_MODE=coverage vendor/bin/phpunit

fix:
	vendor/bin/php-cs-fixer fix -v --
